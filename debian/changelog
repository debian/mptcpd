mptcpd (0.13-2) unstable; urgency=medium

  [ Matthieu Baerts (NGI0) ]
  * debian: proper explanations in README.Debian
  * debian: import upstream patch to support ELL 0.72

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 12 Feb 2025 20:44:30 +0100

mptcpd (0.13-1) unstable; urgency=medium

  [ Matthieu Baerts ]
  * New upstream version
    + mptcpd now supports ELL 0.68
    + improve documentation for address notification flag "check_route"
    + plugins can announce an address without a listener
    + new script mptcp-get-debug to collect info for bug reports
    + avoid mptcpwrap test segfault if /etc/protocols is missing
  * update symbols
  * add mptcp-get-debug script to the mptcpize package
  * extend copyright years
  * have package mptcpize suggest mptcpd

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 18 Nov 2024 09:16:48 +0100

mptcpd (0.12-5) unstable; urgency=medium

  [ Matthieu Baerts ]
  * add patches to support ELL >= 0.69;
    closes: #1084302, #1081265
  * build-depend on and suggest pkgconf (not pkg-config)
  * stop override lintian regarding missing init.d-script
  * declare compliance with Debian Policy 4.7.0
  * tidy git-buildpackage usage comment

  [ Jonas Smedegaard ]
  * wrap and sort control file and debhelper snippets
  * add myself as uploader

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 29 Oct 2024 10:42:29 +0100

mptcpd (0.12-4) unstable; urgency=medium

  * debian: build-dep on systemd-dev i/o systemd. (Closes: #1060476)
    - Michael Biebl reported that systemd.pc file is now in systemd-dev
      package.
  * debian: add new main sponsor for 2024.
    - NGI0 Core is my current sponsor for this work. Adapting metadata to be
      in sync with my Git's SoB tag.

 -- Matthieu Baerts (NGI0) <matttbe@kernel.org>  Fri, 12 Jan 2024 11:56:26 +0100

mptcpd (0.12-3) unstable; urgency=medium

  * debian: build-dep: add netbase. (Closes: #1060285)
    - Aurelien Jarno found out that the unit tests can crash when SCTP is
      disabled and netbase is not installed. A fix for the crash in the unit
      tests is available upstream, but it is not needed if netbase has been
      added to the list of dependences. This package will also help displaying a
      better warning message.
  * debian: lintian: fix mptcp.service path.
    - The path can be different depending on the architecture, since 0.12-2. The
      lintian override file was not taking this into consideration.

 -- Matthieu Baerts (NGI0) <matttbe@kernel.org>  Thu, 11 Jan 2024 14:55:36 +0100

mptcpd (0.12-2) unstable; urgency=medium

  [ Helmut Grohne ]
  * Fix FTBFS when systemd.pc changes systemdsystemunitdir. (Closes: #1052667)

  [ Matthieu Baerts ]
  * debian: update years in copyright file.
  * debian: update my email address.

 -- Matthieu Baerts <matttbe@kernel.org>  Sun, 22 Oct 2023 19:25:32 +0200

mptcpd (0.12-1) unstable; urgency=medium

  * New upstream version.
    - mptcpd has been transferred to multipath-tcp's GitHub organisation.
    - The payload sizes of path management commands sent to the kernel were
      reduced in some cases, slightly improving performance.
    - Automatically set the "signal" flag when a TCP/IP address with a non-zero
      port is passed to the mptcpd_kpm_set_flags() function. The "signal" flag
      is required when a port is specified.
    - A potential memory violation that occurred when cleaning up resources in
      the case of a failed attempt to track a network address was corrected.
    - Several order of operation problems in the test-commands unit test were
      addressed. The test-commands unit test now succeeds as expected.
    - Skip the listener manager unit test if the kernel doesn't support MPTCP.
  * debian: copyright: the issue with an SPDX entry has been fixed
  * debian: update refs: repo has moved to multipath-tcp organisation on Github
  * debian: rules: remove workarounds for tests: fixed upstream
  * debian: control: Bump Standards-Version to 4.6.2
  * debian: lintian: remove false positive
  * debian: gbp: update TODO list

 -- Matthieu Baerts <matthieu.baerts@tessares.net>  Wed, 08 Feb 2023 14:33:10 +0100

mptcpd (0.11-1) unstable; urgency=medium

  * New upstream version.
    - Support for the user space MPTCP path management generic netlink API in
      the upstream Linux 5.19 kernel.
    - Propagate the "server side" flag to new_connection and
      connection_established plugin operations.
    - Support a new fullmesh address flag in the mptcpd configuration file and
      command line addr-flags configuration option.
    - Allow the mptcpd network monitor unit test to pass if it detects an
      active interface without any IPv4 or v6 addresses.
    - Fix memory leaks that occured when parsing the addr-flags and notify-flags
      entries in the mptcpd configuration.
    - Fix a use-after-free memory error in the mptcpd load-plugins configuration
      code.
    - Improve the underlying mptcpd ID manager hash algorithm.
  * debian: control: set 'Multi-Arch: foreign' for -doc
  * debian: automatically update symbols for v0.11

 -- Matthieu Baerts <matthieu.baerts@tessares.net>  Fri, 16 Dec 2022 19:12:59 +0100

mptcpd (0.10-1) unstable; urgency=medium

  * New (bug-fix) upstream version. Closes: #1014835
    - Inconsistent byte order handling in mptcpd was corrected.
    - A potential memory violation caused when attempting to register NULL
      networking monitoring operations with mptcpd was fixed.
    - Mptcpd now supports gcc 12.
    - ELL 0.45 or greater is supported.
    - Code coverage was further expanded.
    - The mptcpd network monitor supports loopback interface monitoring if
      so desired.
    - Improved support for reproducible builds by disabling HTML
      timestamps in Doxygen generated mptcpd documentation.
    - Some mptcpd unit tests will be skipped rather than allowed to fail
      on hosts running a kernel without MPTCP support.
  * debian: drop no longer needed patches
  * debian: add new symbols from 0.10
  * debian: rules: remove workaround for tests
  * debian: remove note about reprotest issues
  * debian: copyright: remove auto-gen files
  * debian: remove arch specific path
  * debian: rules: force using MPTCP Upstream implem
  * debian: control: switch to v4.6.1.0
  * debian: lintian-overrides: add '[]' around path
  * debian: rules: mark some tests as unstable

 -- Matthieu Baerts <matthieu.baerts@tessares.net>  Tue, 02 Aug 2022 11:24:07 +0200

mptcpd (0.9-1) unstable; urgency=low

  * Initial release. Closes: #1005012
  * Modifications since the first FTP Master review:
    - debian: official Git repo is now on salsa
    - debian: add gbp config
    - debian: doxygen: disable HTML_TIMESTAMP
    - debian: apply 'debmake -P' autogenerated files
    - debian: copyright: missing debian files
    - debian: copyright: mptcp_upstream.h is under GPLv2+
    - debian: readme: reproducible failing due to doxygen
    - debian: readme: link to doxygen issue
    - debian: copyright: note about incorrect SPDX entry

 -- Matthieu Baerts <matthieu.baerts@tessares.net>  Fri, 18 Mar 2022 13:23:05 +0100
